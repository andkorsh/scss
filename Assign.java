
   import java.util.ArrayList;
   import java.util.Collections;

   public class Assign
   {
      /*
       * Функция, вычисляющая полное назначение максимального
       * веса жажным методом. Время - O(k*k*log(k))
       */
       
      private static int[] assignment(int[][] a)
      {
         int n = a.length;
         boolean[][] notallow = new boolean[n][n];
        
         int[] m = new int[n];
        
         while (true)
         {
            int max = -1, maxi = -1, maxj = -1;
          
            for (int i = 0; i < n; i++)
            {
               for (int j = 0; j < n; j++)
               {
                  if (notallow[i][j]) continue;
                  if (a[i][j] > max)
                  {
                     max = a[i][j];
                     maxi = i; maxj = j;
                  }
               }
            }
          
            if (max == -1) break;
          
            m[maxi] = maxj;
          
            for (int i = 0; i < n; i++)
            {
               notallow[maxi][i] = true;
               notallow[i][maxj] = true;
            }
         }
         return m;
      }
      

      /* Функция, конструирующая кратчайшую надстроку */
      public static String createSuperString(ArrayList<String> strings)
      {
         int n = strings.size();
        
         // Вычисление матрицы overlap'ов для всех 
         // упорядоченных пар (Si, Sj).
         int[][] overlaps = new int[n][n];
         for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
               overlaps[i][j] = overlap(strings.get(i), strings.get(j));

         int[] assign = assignment(overlaps);

         // Нахождение покрытия циклами минимальной полной длины
         // для полного назначения assign
         ArrayList<ArrayList<Integer>> cycles 
            = new ArrayList<ArrayList<Integer>>();
         ArrayList<Integer> cycle = new ArrayList<Integer>();
         boolean[] mark = new boolean[assign.length];
        
         for (int i = 0; i < assign.length; i++)
         {
            if (mark[i]) continue;
          
            cycle.add(i);
            mark[i] = true;
          
            if (assign[i] == cycle.get(0))
            {
               cycles.add(cycle);
               cycle = new ArrayList<Integer>();
               i = 0;
            }
            else
            {
               i = assign[i] - 1;
            }
         }

         // Циклический сдвиг каждого цикла в покрытии такой, чтобы
         // минимизировать overlap первой и последней строки в цикле
         // и конструирование надстроки для каждого цикла.
         ArrayList<String> superstrings = new ArrayList<String>();
         for (ArrayList<Integer> c : cycles)
         {
            String str = "";
            ArrayList<Integer> ovs = new ArrayList<Integer>();
          
            for (int i = 0; i < c.size() - 1; i++)
               ovs.add(overlaps[c.get(i)][c.get(i+1)]);
          
            int min = overlaps[c.get(c.size()-1)][c.get(0)];
            int shift = 0;
          
            for (int i = 0; i < ovs.size(); i++)
            if (ovs.get(i) < min) 
            {
               min = ovs.get(i);
               shift = i + 1;
            }
          
            Collections.rotate(c, -shift);
          
            for (int i = 0; i < c.size() - 1; i++)
               str += prefix(strings.get(c.get(i)), 
                          overlaps[c.get(i)][c.get(i+1)]);
               str += strings.get(c.get(c.size()-1));
            superstrings.add(str);
         }
        

         // Конкатенация всех надстрок из superstrings
         StringBuilder superstring = new StringBuilder();
         for (String str : superstrings)
            superstring.append(str);
        
         return superstring.toString();
      }

      /*
       * Функция возвращает строку s1, обрезанную справа
       * на ov символов
       */
      private static String prefix(String s1, int ov)
      {
         return s1.substring(0, s1.length() - ov);
      }

      /*
       * Функция вычисляет максимальную длину суффикса строки s1
       * совпадающего с префиксом строки s2 (длину наложения s1 на s2)
       */
      private static int overlap(String s1, String s2)
      {
         int s1last = s1.length() - 1;
         int s2len = s2.length();
         int overlap = 0;
         for (int i = s1last, j = 1; i > 0 && j < s2len; i--, j++)
         {
            String suff = s1.substring(i);
            String pref = s2.substring(0, j);
            if (suff.equals(pref)) overlap = j; 
         }
         return overlap;
      }
   }
