
   import java.io.File;
   import java.io.FileNotFoundException;
   import java.io.FileWriter;
   import java.io.IOException;
   import java.util.ArrayList;
   import java.util.Scanner;
   import java.util.Random;

   public class Main
   {
      private static String inputFileName;
      private static String outputFileName = "output.txt";
      private static int compNumber;
      
      private static int test(ArrayList<String> substrings, String superstring)
      {
         int errors = 0;
         
         for (String substring : substrings)
            if (superstring.indexOf(substring) < 0)
               errors++;
           return errors;
      }
      
      private static String getRandomInput(int s, int c, boolean f, int a)
      {
         StringBuilder result = new StringBuilder();
         Random random = new Random(System.currentTimeMillis());
         while (s-- > 0)
         {
            int n = f ? c : random.nextInt(c);
            while (n-- >= 0)
               result.append((char) (random.nextInt(a) + 'a'));
            result.append('\n');
         }
         return result.toString();
      }

      @SuppressWarnings("unchecked")
      public static void main(String... args)
      {
         ArrayList<String> input = new ArrayList<String>();
         Scanner scanner = null;
         
         try
         {     
            scanner = new Scanner(System.in);
            System.out.print("The number of computations    : ");
            compNumber = scanner.nextInt();
            scanner.nextLine();
            System.out.print("Input file name (or 'random') : ");
            inputFileName = scanner.nextLine();
            
            if (inputFileName.equals("random"))
            {
               System.out.print("The number of strings         : ");
               int s = scanner.nextInt();
               System.out.print("Max length of strings         : ");
               int c = scanner.nextInt();
               System.out.print("Fix length? (0 or 1)          : ");
               boolean f = scanner.nextInt() == 1;
               System.out.print("Size of the alphabet          : ");
               int a = scanner.nextInt();
               
               scanner = new Scanner(getRandomInput(s, c, f, a));
            }
            else
            {
                scanner = new Scanner(new File(inputFileName));
            }
            while (scanner.hasNext())
               input.add(scanner.nextLine());
         }
         catch (FileNotFoundException ex)
         {
            System.out.print("Input file '" + inputFileName); 
            System.out.println("' was not found.");
            System.exit(1);
         }
         catch (Exception ex)
         {
            System.out.println("Input exception.");
            System.exit(1);
         }
         finally
         {
            if (scanner != null)
               scanner.close();
         }
         
         System.out.println("\n --- start --- \n");
         
         long greedyTime = 0, assignTime = 0;
         String greedyResult = "", assignResult = ""; 
         
         for (int i = 0; i < compNumber; i++)
         {
            long time = System.currentTimeMillis();
            greedyResult = Greedy.createSuperString
               ((ArrayList<String>) input.clone());
            time = System.currentTimeMillis() - time;
            greedyTime += time;
            System.out.println("Greedy - comp " + (i+1) + " completed");
         }
         greedyTime /= compNumber;

         for (int i = 0; i < compNumber; i++)
         {
            long time = System.currentTimeMillis();
            assignResult = Assign.createSuperString
               ((ArrayList<String>) input.clone());
            time = System.currentTimeMillis() - time;
            assignTime += time;
            System.out.println("Assign - comp " + (i+1) + " completed");
         }
         assignTime /= compNumber;
         
         FileWriter writer = null;
         try
         {
            writer = new FileWriter(new File(outputFileName));
            
            writer.write("Входные строки:\n\n");
            int commonLen = 0;
            for (String str: input)
            {
               writer.write("   " + str + "\n");
               commonLen += str.length();
            }
            writer.write("\nОбщая длина набора : " + commonLen + 
                         " символов\n\n");
            
            writer.write("Алгоритм жадного слияния:\n\n   ");
            writer.write(greedyResult+"\n");
            writer.write("\n   Длина надстроки  : " + 
                         greedyResult.length() + " символов");
            writer.write("\n   Выйгрыш          : " + 
                         (commonLen - greedyResult.length()) + " символов");
            writer.write("\n   Результаты теста : " + 
                         test(input, greedyResult) + " ошибок");
            writer.write("\n   Среднее время    : " + 
                         greedyTime + " мс");
            
            writer.write("\n\nАлгоритм Блюма-Янга-Ли-Тромпа-Яннакакиса:\n\n   ");
            writer.write(assignResult+"\n");
            writer.write("\n   Длина надстроки  : " + 
                          assignResult.length() + " символов");
            writer.write("\n   Выйгрыш          : " + 
                         (commonLen - assignResult.length()) + " символов");
            writer.write("\n   Результаты теста : " + 
                          test(input, assignResult) + " ошибок");
            writer.write("\n   Среднее время    : " + 
                          assignTime + " мс");
         }
         catch (IOException ex)
         {
            System.out.print("Error while writing to file ");
            System.out.println("'" + outputFileName + "'.");
         }
         finally
         {
            try
            {
               if (writer != null)
                  writer.close();
            }
            catch (Exception ex) {}
         }
         
         System.out.println("\n --- finish ---");
      }
   }
